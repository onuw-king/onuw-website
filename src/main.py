from sys import argv
from uuid import uuid4
from datetime import (
    datetime,
    timedelta
)
from asyncio import run
from fastapi import (
    FastAPI,
    Request,
    Response,
    Header,
    Cookie
)
from fastapi.responses import (
    HTMLResponse,
    RedirectResponse,
    JSONResponse
)
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from hypercorn.config import Config
from hypercorn.asyncio import serve
from hypercorn.middleware import HTTPToHTTPSRedirectMiddleware
from aiohttp import ClientSession

from datastructs import (
    User,
    UserManager,
    proper_utc
)

#input args
#   1. TOML filename
#   2. discord client ID
#   3. discord client secret
#   4(opt). is dev instance

toml_filename = str(argv[1])
disc_client_id = str(argv[2])
disc_client_secret = str(argv[3])

is_dev = False
domain = "https://onuwbot.com"
discord_login_url = "https://discord.com/api/oauth2/authorize?client_id=714448635907014676&redirect_uri=https%3A%2F%2Fonuwbot.com%2Fauth%2Fdiscord%2Flogin%2Fredirect&response_type=code&scope=identify%20guilds"
discord_bot_join_url = "https://discord.com/api/oauth2/authorize?client_id=714448635907014676&permissions=3072&redirect_uri=https%3A%2F%2Fonuwbot.com%2Fauth%2Fdiscord%2Flogin%2Fredirect&response_type=code&scope=identify%20guilds%20bot"
use_secure_cookies = True
if "--dev" in argv:
    is_dev = True
    use_secure_cookies = False
    domain = "http://0.0.0.0:8000"
    discord_login_url = "https://discord.com/api/oauth2/authorize?client_id=714448635907014676&redirect_uri=http%3A%2F%2F0.0.0.0%3A8000%2Fauth%2Fdiscord%2Flogin%2Fredirect&response_type=code&scope=identify%20guilds"
    discord_bot_join_url = "https://discord.com/api/oauth2/authorize?client_id=714448635907014676&permissions=3072&redirect_uri=http%3A%2F%2F0.0.0.0%3A8000%2Fauth%2Fdiscord%2Flogin%2Fredirect&response_type=code&scope=identify%20guilds%20bot"

discord_base_url = "https://discord.com/api"
discord_base_img_url = "https://cdn.discordapp.com"
discord_api_url = f"{discord_base_url}/v8"
discord_oauth2_url = f"{discord_base_url}/oauth2"
discord_token_url = f"{discord_oauth2_url}/token"
discord_token_revoke_url = f"{discord_token_url}/revoke"
discord_cur_user_url = f"{discord_api_url}/users/@me"
discord_user_av_url = f"{discord_base_img_url}/avatars"

page_urls = {
    "home_page_url": "/",
    "bot_page_url": "/bot",
    "commands_page_url": "/commands",
    "config_page_url": "/game-configurations",
    "donate_page_url": "/",
    "discord_oauth2_login_url": "/auth/discord/login",
    "discord_oauth2_redirect_url": "/auth/discord/login/redirect",
    "discord_oauth2_logout_url": "/auth/discord/logout",
    "discord_oauth2_join_url": "/auth/discord/join"
}

web_config = Config.from_toml(f"/app/config/{toml_filename}")
api = FastAPI()
user_man = UserManager()
discord_ses = None

templates = Jinja2Templates(directory="templates")

@api.on_event("startup")
async def startup_sessions():
    global discord_ses
    discord_ses = ClientSession()

@api.on_event("shutdown")
async def shutdown_sessions():
    await discord_ses.close()

@api.get("/", response_class=HTMLResponse)
async def home(request: Request, user_id: str = Cookie(None)):
    ctx = {
        "request": request,
        **page_urls,
        "user_exists": False
    }
    if user_id:
        user = user_man.get_user(user_id)
        if not(user is None):
            ctx["user_exists"] = True
            ctx["user_avatar_url"] = user.get_avatar_url(discord_user_av_url)
            ctx["user_name"] = user.data["user"]["username"]
    return templates.TemplateResponse("index.html", ctx)

@api.get("/bot", response_class=HTMLResponse)
async def bot(request: Request, user_id: str = Cookie(None)):
    ctx = {
        "request": request,
        **page_urls,
        "user_exists": False
    }
    if user_id:
        user = user_man.get_user(user_id)
        if not(user is None):
            ctx["user_exists"] = True
            ctx["user_avatar_url"] = user.get_avatar_url(discord_user_av_url)
            ctx["user_name"] = user.data["user"]["username"]
    return templates.TemplateResponse("bot.html", ctx)

@api.get("/commands", response_class=HTMLResponse)
async def commands(request: Request, user_id: str = Cookie(None)):
    ctx = {
        "request": request,
        **page_urls,
        "user_exists": False
    }
    if user_id:
        user = user_man.get_user(user_id)
        if not(user is None):
            ctx["user_exists"] = True
            ctx["user_avatar_url"] = user.get_avatar_url(discord_user_av_url)
            ctx["user_name"] = user.data["user"]["username"]
    return templates.TemplateResponse("commands.html", ctx)

@api.get("/game-configurations", response_class=HTMLResponse)
async def game_configs(request: Request, user_id: str = Cookie(None)):
    ctx = {
        "request": request,
        **page_urls,
        "user_exists": False
    }
    if user_id:
        user = user_man.get_user(user_id)
        if not(user is None):
            ctx["user_exists"] = True
            ctx["user_avatar_url"] = user.get_avatar_url(discord_user_av_url)
            ctx["user_name"] = user.data["user"]["username"]
    return templates.TemplateResponse("game_configs.html", ctx)

@api.get("/auth/discord/login", response_class=RedirectResponse)
async def discord_oauth2_login(request: Request):
    state = str(uuid4().hex)
    return RedirectResponse(f"{discord_login_url}&state={state}")

@api.get("/auth/discord/join", response_class=RedirectResponse)
async def discord_oauth2_join(request: Request):
    state = str(uuid4().hex)
    return RedirectResponse(f"{discord_bot_join_url}&state={state}")

@api.get("/auth/discord/login/redirect", response_class=RedirectResponse)
async def discord_oauth2_login_redirect(code: str = None, state: str = None, user_id: str = Cookie(None)):
    if (
        code is None
        or state is None
        or user_id
    ):
        return RedirectResponse(f"{domain}")
    else:
        user = await get_discord_user(code, state)
        await user_man.add_user(user)

        resp = RedirectResponse(f"{domain}")
        #force an early expiration to account for any time differences
        exp_time = user.data["security"]["expires_in"] - 30
        resp.set_cookie(key="user_id", value=user.id, expires=exp_time, secure=use_secure_cookies)
        return resp

@api.get("/auth/discord/logout", response_class=RedirectResponse)
async def discord_oauth2_logout_redirect(user_id: str = Cookie(None)):
    await user_man.logout(user_id, discord_ses, discord_token_revoke_url)
    resp = RedirectResponse(f"{domain}")
    resp.set_cookie(key="user_id", value=None, expires=1, secure=use_secure_cookies)
    return resp

async def get_discord_user(code: str, state: str):
    last_visited = proper_utc(datetime.utcnow())

    #keep this here because this allows logout to always be successful prior to this time period because
    token_expires_at = proper_utc(datetime.utcnow())

    #get security data
    data = {
        "client_id": disc_client_id,
        "client_secret": disc_client_secret,
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": f"{domain}{page_urls['discord_oauth2_redirect_url']}",
        "scope": "identify"
    }
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    resp = await discord_ses.post(discord_token_url, data=data, headers=headers)
    creds = await resp.json()

    #get user data
    resp = await discord_ses.get(discord_cur_user_url, headers={"Authorization": f"Bearer {creds['access_token']}"})
    user_data = await resp.json()
    user_data["state"] = state
    token_expires_at += timedelta(seconds=creds["expires_in"])
    return User(user_data, creds, last_visited, token_expires_at, disc_client_id, disc_client_secret)

if is_dev:
    run(serve(api, web_config))
else:
    redir_api = HTTPToHTTPSRedirectMiddleware(api, host="onuwbot.com")
    run(serve(redir_api, web_config))