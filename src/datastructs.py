from uuid import uuid4
from datetime import (
    datetime,
    timezone,
    timedelta
)
from aiohttp import ClientSession

def proper_utc(dt_utc: datetime):
    return dt_utc.replace(tzinfo=timezone.utc)

class User:
    def __init__(self,
        user_data: dict,
        security_data: dict,
        last_visited: datetime,
        expires_at: datetime,
        disc_client_id: str,
        disc_secret: str
    ):
        self.data = {
            "user": user_data,
            "security": security_data
        }
        self.id = uuid4().hex
        self.last_visited = last_visited
        self.expires_at = expires_at
        self.disc_client_id = disc_client_id
        self.disc_secret = disc_secret
    
    def __eq__(self, other):
        if not(isinstance(other, User)):
            return False
        return self.data["user"]["id"] == other.data["user"]["id"]
    
    def __ne__(self, other):
        return not(self.__eq__(other))

    def __hash__(self):
        return hash(self.id)

    def get_avatar_url(self, base_url: str, size: int = None):
        file_ext = ".png"
        av_hash = self.data["user"]["avatar"]
        uid = self.data["user"]["id"]

        # In the case of endpoints that support GIFs, the hash will
        # begin with a_ if it is available in GIF format.
        # (example: a_1269e74af4df7417b13759eae50c83dc)
        #ref: https://discord.com/developers/docs/reference#image-formatting
        if "a_" in av_hash:
            file_ext = ".gif"

        if size is None:
            return f"{base_url}/{uid}/{av_hash}{file_ext}"
        else:
            return f"{base_url}/{uid}/{av_hash}{file_ext}?size={size}"

    async def logout(self, s: ClientSession, revoke_url: str):
        data = {
            "client_id": self.disc_client_id,
            "client_secret": self.disc_secret,
            "token": self.data["security"]["refresh_token"],
            "token_type_hint": "refresh_token"
        }
        headers = {
            "Content-Type": "application/x-www-form-urlencoded"
        }
        resp = await s.post(revoke_url, data=data, headers=headers)

class UserManager:
    def __init__(self):
        self.users = {}

    async def logout(self, uid: str, s: ClientSession, revoke_url: str):
        await self.users[uid].logout(s, revoke_url)
        self.users.pop(uid, None)

    async def logout_all(self):
        for user in self.users.values():
            await user.logout()
        self.users = {}

    async def add_user(self, user: User):
        if user in self.users:
            client_id = user.data["user"]["id"]
            old_user = [u for u in self.users.values() if u == user][0]
            #if security data is expired, delete the old user and then add the new
            #one; otherwise, keep the old user.
            if old_user.expires_at < proper_utc(datetime.utcnow()):
                self.users.pop(old_user.id, None)
                self.users[user.id] = user
        else:
            self.users[user.id] = user

    def get_user(self, user_id: str):
        if self.exists(user_id):
            old_user = [u for u in self.users.values() if u.id == user_id][0]
            #if security data is relevant, return the existing user.
            if old_user.expires_at > proper_utc(datetime.utcnow()):
                return old_user
        return None


    def exists(self, user_id: str):
        for u in self.users.values():
            if user_id == u.id:
                return True
        return False