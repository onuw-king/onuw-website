FROM python:3.8-slim
ENV APP="/app"
ENV INSTALL="/install"
ENV LOC="/root/.local"
ENV PATH $PATH:${LOC}:${LOC}/bin

#install all dependencies
COPY ./requirements.txt ${APP}/requirements.txt
RUN pip3 install --user --no-cache-dir -r ${APP}/requirements.txt
RUN mkdir ${APP}/config
RUN mkdir ${APP}/web_certs

COPY ./src/ ${APP}
COPY ./src/static/bootstrap/css ${APP}/static/bootstrap/css
COPY ./src/static/bootstrap/js ${APP}/static/bootstrap/js
COPY ./src/templates/ ${APP}/templates

WORKDIR ${APP}
EXPOSE 443
EXPOSE 80
EXPOSE 8000
#start the service
ENTRYPOINT [ "python3", "main.py" ]